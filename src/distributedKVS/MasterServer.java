package distributedKVS;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class MasterServer {

    private static final int LISTEN_PORT = 5000;
    private static int SLAVE_ID_COUNTER = 0;
    private static Map<Integer, Socket> slaveServers = new HashMap<>();
    private static final Object semaphore = new Object();

    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(LISTEN_PORT);
            while (true) {
                Socket socket = serverSocket.accept(); // block
                new Thread(() -> {
                    try {
                        Scanner scanner = new Scanner(socket.getInputStream());
                        PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
                        String cmd = scanner.nextLine();
                        if (cmd.startsWith("CHIN::")) {
                            String address = socket.getInetAddress().toString();
                            int slaveListenPort = Integer.parseInt(cmd.split("::")[1]);
                            Socket masterToSlaveSocket = new Socket(address, slaveListenPort);
                            int slaveId = generateSlaveId();
                            addSlave(slaveId, masterToSlaveSocket);
                            writer.write("ACK::" + slaveId);
                            if (scanner.nextLine().startsWith("SRAND::")) {
                                if (countSlaves() == 0) {
                                    writer.write("EMPTY");
                                } else {
                                    Random random = new Random();
                                    int randId;
                                    while ((randId = random.nextInt(countSlaves())) != slaveId) ;
                                    Socket s = getSlave(randId);
                                    String randomSlave = s.getInetAddress().toString() + ":" + s.getPort();
                                    writer.write(randomSlave);
                                }
                            }
                        } else if (cmd.startsWith("CHOUT::")) {
                            int slaveId = Integer.parseInt(cmd.split("::")[1]);
                            removeSlave(slaveId);
                            writer.write("ACK");
                        } else if (cmd.startsWith("BCAST::")) {
                            String[] parts = cmd.split("::");
                            int slaveId = Integer.parseInt(parts[1]);
                            String kayValue = parts[2];
                            MasterServer.sendKeyValueBroadcast(kayValue, slaveId);
                            writer.write("ACK");
                        } else if (cmd.startsWith("SRAND")) {
                            Random random = new Random();
                            int randId;
                            if (cmd.equalsIgnoreCase("SRAND")) {
                                randId = random.nextInt(countSlaves());
                            } else { // req came from slave server in the form SRAND::slaveId
                                int slaveId = Integer.parseInt(cmd.split("::")[1]);
                                while ((randId = random.nextInt(countSlaves())) != slaveId) ;
                            }
                            Socket s = getSlave(randId);
                            String randomSlave = s.getInetAddress().toString() + ":" + s.getPort();
                            writer.write(randomSlave);
                        } else {
                            // ne zafrkava ha-ha
                        }
                        scanner.close();
                        writer.close();
                        socket.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    static void addSlave(int slaveId, Socket masterToSlaveSocket) {
        synchronized (semaphore) {
            slaveServers.put(slaveId, masterToSlaveSocket);
        }
    }

    static void removeSlave(int slaveId) {
        synchronized (semaphore) {
            slaveServers.remove(slaveId);
        }
    }

    static Socket getSlave(int slaveId) {
        synchronized (semaphore) {
            return slaveServers.get(slaveId);
        }
    }

    static void sendKeyValueBroadcast(String keyValue, int exceptId) {
        synchronized (semaphore) {
            slaveServers.entrySet()
                    .stream()
                    .filter(entry -> !entry.getKey().equals(exceptId))
                    .forEach(entry -> {
                        Socket socket = entry.getValue();
                        try {
                            PrintWriter writer = new PrintWriter(socket.getOutputStream());
                            String msg = "MPUTKV" + keyValue;
                            writer.write(msg);
                            writer.close();
                            // do not close the socket !
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    });
        }
    }

    static synchronized int generateSlaveId() {
        return SLAVE_ID_COUNTER++;
    }

    static int countSlaves() {
        synchronized (semaphore) {
            return slaveServers.size();
        }
    }
}