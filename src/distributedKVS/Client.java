package distributedKVS;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private static final String MASTER_SERVER_ADDRESS = "localhost";
    private static final int MASTER_SERVER_PORT = 5000;

    public static void main(String[] args) {
        try {
            Socket socket = new Socket(InetAddress.getByName(MASTER_SERVER_ADDRESS), MASTER_SERVER_PORT);
            Scanner scanner = new Scanner(socket.getInputStream());
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            writer.write("SRAND");
            String randomSlave = scanner.nextLine();

            String[] parts = randomSlave.split(":");
            String address = parts[0];
            int port = Integer.parseInt(parts[1]);

            Socket s = new Socket(InetAddress.getByName(address), port);
            Scanner sc = new Scanner(socket.getInputStream());
            PrintWriter wr = new PrintWriter(socket.getOutputStream(), true);
            wr.write("GETV::" + "monkey");
            String value = scanner.nextLine();
            System.out.println(value);

            wr.write("PUTKV::martin:kotevski");
            String ack = sc.nextLine(); // should be ACK

            sc.close();
            wr.close();
            s.close();

            scanner.close();
            writer.close();
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
