package distributedKVS;

import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class SlaveServer {

    private static Map<String, String> pairs = new HashMap<>();
    private static final Object sem = new Object();
    public static final String MASTER_ADDRESS = "localhost";
    public static final int MASTER_PORT = 5000;
    public static int ID;
    public static int LISTEN_PORT = 8000;

    public static void main(String[] args) {
        try {
            Socket socket = new Socket(InetAddress.getByName(MASTER_ADDRESS), MASTER_PORT);
            Scanner scanner = new Scanner(socket.getInputStream());
            PrintWriter writer = new PrintWriter(socket.getOutputStream());
            writer.write("CHIN::" + LISTEN_PORT);
            String response = scanner.nextLine();
            if (response.startsWith("ACK::")) {
                ID = Integer.parseInt(response.split("::")[1]);
                writer.write("SRAND::" + ID);
                String randomSlave = scanner.nextLine();
                if (!randomSlave.equalsIgnoreCase("EMPTY")) {
                    String[] parts = randomSlave.split(":");
                    String address = parts[0];
                    int port = Integer.parseInt(parts[1]);
                    Socket s = new Socket(address, port);
                    Scanner sc = new Scanner(s.getInputStream());
                    PrintWriter wr = new PrintWriter(s.getOutputStream());
                    wr.write("LIST");
                    String pairsFlattened = sc.nextLine();
                    String[] pairs = pairsFlattened.split(";");
                    for (String pair : pairs) {
                        String[] kv = pair.split(":");
                        addKeyValuePair(kv[0], kv[1]);
                    }
                }
            }
            scanner.close();
            writer.close();
            socket.close();

            new Listener().start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static void addKeyValuePair(String key, String value) {
        synchronized (sem) {
            pairs.put(key, value);
        }
    }

    static String getValue(String key) {
        synchronized (sem) {
            return pairs.get(key);
        }
    }

    static String getPairsFlattened() {
        synchronized (sem) {
            return pairs.entrySet().stream().map(e -> e.getKey() + ":" + e.getValue()).collect(Collectors.joining(";"));
        }
    }

}

class Listener extends Thread {

    @Override
    public void run() {
        try {
            ServerSocket serverSocket = new ServerSocket(SlaveServer.LISTEN_PORT);
            while (true) {
                Socket socket = serverSocket.accept();
                new Thread(() -> {
                    try {
                        Scanner scanner = new Scanner(socket.getInputStream());
                        PrintWriter writer = new PrintWriter(socket.getOutputStream());
                        String cmd = scanner.nextLine();
                        if (cmd.startsWith("MPUTKV::")) { // comes from MASTER
                            String keyValue = cmd.split("::")[1];
                            String[] pair = keyValue.split(":");
                            SlaveServer.addKeyValuePair(pair[0], pair[1]);
                            writer.write("ACK");
                        } else if (cmd.equalsIgnoreCase("LIST")) {
                            writer.write(SlaveServer.getPairsFlattened());
                        } else if (cmd.startsWith("PUTKV::")) {
                            String keyValue = cmd.split("::")[1];
                            String[] pair = keyValue.split(":");
                            SlaveServer.addKeyValuePair(pair[0], pair[1]);
                            writer.write("ACK");
                            Socket s = new Socket(InetAddress.getByName(SlaveServer.MASTER_ADDRESS), SlaveServer.MASTER_PORT);
                            PrintWriter w = new PrintWriter(s.getOutputStream());
                            w.write("BCAST::" + SlaveServer.ID + "::" + keyValue);
                            w.close();
                        } else if (cmd.startsWith("GETV::")) {
                            String key = cmd.split("::")[1];
                            String value = SlaveServer.getValue(key);
                            writer.write(value);
                        }
                        scanner.close();
                        writer.close();
                        socket.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }).start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
