package kvs;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class SlaveServer {

    private int ID;
    private final Map<String, String> pairs = new HashMap<>();
    private final Object sem = new Object();

    private final String masterAddress;
    private final int masterPort;
    private Socket masterCommunicationSocket;

    private final int listenPortForMaster;
    private ServerSocket serverSocketForMaster;
    private ServerSocket serverSocketForClient;

    public SlaveServer(String masterAddress, int masterPort, int listenPortForMaster) throws InterruptedException {
        this.masterAddress = masterAddress;
        this.masterPort = masterPort;
        this.listenPortForMaster = listenPortForMaster;
        int tryCount = 0;
        while (++tryCount <= 5) {
            try {
                this.masterCommunicationSocket = new Socket(InetAddress.getByName(masterAddress), masterPort);
            } catch (IOException e) {
                e.printStackTrace();
                Thread.sleep(5 * 5000);
            }
        }
    }

    public void init() {
        try {
            PrintWriter writer = new PrintWriter(masterCommunicationSocket.getOutputStream(), true);
            Scanner scanner = new Scanner(masterCommunicationSocket.getInputStream());
            writer.println("CHECKIN::" + listenPortForMaster);
            String response = scanner.nextLine();
            if (response.startsWith("ACK::")) {
                ID = Integer.parseInt(response.split("::")[1]);
                int exceptId = ID;
                writer.println("RANDOM::" + exceptId);
                response = scanner.nextLine();
                if (response.startsWith("ACK::")) {
                    String slave = response.split("::")[1];
                    String parts[] = slave.split(":");
                    String slaveAddress = parts[0];
                    int port = Integer.parseInt(parts[1]);
                    Socket s = new Socket(InetAddress.getByName(slaveAddress), port);
                    PrintWriter wr = new PrintWriter(s.getOutputStream(), true);
                    Scanner sc = new Scanner(s.getInputStream());
                    writer.println("LIST");
                    response = sc.nextLine();
                    if (response.startsWith("ACK::")) {
                        String listFlattened = response.split("::")[1];
                        String[] pairs = listFlattened.split(";");
                        for (String pair : pairs) {
                            String[] pts = pair.split("->");
                            putPair(pts[0], pts[1]);
                        }
                    }
                }
            }
            writer.close();
            scanner.close();
            // do not close the masterCommunicationSocket
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Thread createMasterListener() throws IOException {
        serverSocketForMaster = new ServerSocket(listenPortForMaster);
        return new Thread(() -> {
            try {
                Socket communicationSocket = serverSocketForMaster.accept();
                PrintWriter writer = new PrintWriter(communicationSocket.getOutputStream(), true);
                Scanner scanner = new Scanner(communicationSocket.getInputStream());
                while (true) {
                    if (scanner.hasNextLine()) {
                        String cmd = scanner.nextLine();
                        if (cmd.startsWith("PUTKV::")) {
                            String pair = cmd.split("::")[1];
                            String[] parts = pair.split("->");
                            String key = parts[0];
                            String value = parts[1];
                            putPair(key, value);
                            writer.println("ACK");
                        } else {
                            writer.println("NACK");
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public Thread createClientListener(int port) throws IOException {
        serverSocketForClient = new ServerSocket(port);
        return new Thread(() -> {
            try {
                while (true) {
                    Socket socket = serverSocketForClient.accept();
                    new Thread(() -> {
                        try {
                            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
                            Scanner scanner = new Scanner(socket.getInputStream());
                            if (scanner.hasNextLine()) {
                                String cmd = scanner.nextLine();
                                if (cmd.startsWith("GETV::")) { // get value
                                    String key = cmd.split("::")[1];
                                    String val = getValue(key);
                                    writer.println("ACK::" + val);
                                } else if (cmd.startsWith("PUTKV")) { // put key-value pair
                                    String pair = cmd.split("::")[1];
                                    String[] parts = pair.split("->");
                                    String key = parts[0];
                                    String value = parts[1];
                                    putPair(key, value);
                                    writer.println("ACK");
                                    // send key-value pair to master
                                    PrintWriter wr = new PrintWriter(masterCommunicationSocket.getOutputStream(), true);
                                    Scanner sc = new Scanner(masterCommunicationSocket.getInputStream());
                                    int exceptId = this.ID;
                                    writer.println("NOTIFY::" + this.ID + "::" + pair);
                                } else if (cmd.equalsIgnoreCase("LIST")) { // get list of key-value pairs
                                    writer.println("ACK::" + getPairsFlattened());
                                } else {
                                    writer.println("NACK"); // wrong command - not acknowledged
                                }
                            }
                            writer.close();
                            scanner.close();
                            socket.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }).start();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private String getValue(String key) {
        synchronized (sem) {
            return pairs.get(key);
        }
    }

    private void putPair(String key, String value) {
        synchronized (sem) {
            pairs.put(key, value);
        }
    }

    private String getPairsFlattened() {
        synchronized (sem) {
            return pairs.entrySet()
                    .stream()
                    .map(entry -> entry.getKey() + "->" + entry.getValue())
                    .collect(Collectors.joining(";"));
        }
    }
}


