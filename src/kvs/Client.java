package kvs;

import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private final String masterAddress;
    private final int masterPort;

    public Client(String masterAddress, int masterPort) {
        this.masterAddress = masterAddress;
        this.masterPort = masterPort;
    }

    


    public static void main(String[] args) {
        try {
            InetAddress localhost = InetAddress.getLocalHost();
            int masterPort = 5001;

            Socket masterSocket = new Socket(localhost, masterPort);
            PrintWriter writer = new PrintWriter(masterSocket.getOutputStream(), true);
            Scanner scanner = new Scanner(masterSocket.getInputStream());
            writer.println("RANDOM");
            String response = scanner.nextLine();
            if (response.startsWith("ACK::")) {
                String slave = response.split("::")[1];
                String[] parts = slave.split(":");
                String address = parts[0];
                int port = Integer.parseInt(parts[1]);
                Socket s = new Socket(InetAddress.getByName(address), port);
                PrintWriter wr = new PrintWriter(s.getOutputStream(), true);
                Scanner sc = new Scanner(s.getInputStream());
                wr.println("PUTKV::animal->monkey");
                String res = sc.nextLine();
                if (res.equalsIgnoreCase("ACK")) {
                    wr.println("GETV::animal");
                    res = sc.nextLine();
                    if (res.startsWith("ACK::")) {
                        System.out.println("The value for animal is: " + res.split("::")[1]);
                    }
                }
            } else if (response.equals("EMPTY")) {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
