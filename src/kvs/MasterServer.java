package kvs;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadPoolExecutor;

public class MasterServer {

    private ThreadPoolExecutor threadPoolExecutor;
    private static int SLAVE_ID_COUNTER = 0;
    private final Map<Integer, Socket> slaves = new HashMap<>();
    private final Object sem = new Object();

    private ServerSocket serverSocketForSlave;
    private ServerSocket serverSocketForClient;

    public Thread createSlaveListener(int port) throws IOException {
        serverSocketForSlave = new ServerSocket(port);
        return new Thread(() -> {
            try {
                while (true) {
                    Socket socket = serverSocketForSlave.accept();
                    PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
                    Scanner scanner = new Scanner(socket.getInputStream());
                    new Thread(() -> {
                        try {
                            boolean shouldClose = false;
                            while (!shouldClose) {
                                if (scanner.hasNextLine()) {
                                    String cmd = scanner.nextLine();
                                    if (cmd.startsWith("CHECKIN")) {
                                        int slaveId = generateSlaveId();
                                        InetAddress slaveAddress = socket.getInetAddress();
                                        int slaveListeningPort = Integer.parseInt(cmd.split("::")[1]);
                                        Socket communicationSocket = new Socket(slaveAddress, slaveListeningPort);
                                        addSlave(slaveId, communicationSocket);
                                        writer.println("ACK::" + slaveId);
                                    } else if (cmd.startsWith("CHECKOUT")) {
                                        int slaveId = Integer.parseInt(cmd.split("::")[1]);
                                        removeSlave(slaveId);
                                        writer.println("ACK");
                                        shouldClose = true;
                                    }
                                    if (cmd.startsWith("RANDOM")) { // RANDOM::exceptId
                                        int count = countSlaves();
                                        if (count == 0) {
                                            writer.println("EMPTY");
                                        } else {
                                            int exceptId = Integer.parseInt(cmd.split("::")[1]);
                                            Random rand = new Random();
                                            int randomId;
                                            while ((randomId = rand.nextInt(count)) != exceptId) ;
                                            Socket s = getSlave(randomId);
                                            String slave = s.getInetAddress().toString() + ":" + s.getPort(); // ipaddress::port
                                            writer.println("ACK::" + slave);
                                        }
                                    } else if (cmd.startsWith("NOTIFY")) {
                                        String[] parts = cmd.split("::");
                                        int exceptId = Integer.parseInt(parts[1]);
                                        String keyValue = parts[2]; // key->value
                                        sendBroadcast(keyValue, exceptId);
                                        writer.println("ACK");
                                    } else {
                                        writer.println("NOK");
                                    }
                                }
                            }
                            writer.close();
                            scanner.close();
                            socket.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }).start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public Thread createClientListener(int port) throws IOException {
        serverSocketForClient = new ServerSocket(port);
        return new Thread(() -> {
            try {
                while (true) {
                    Socket socket = serverSocketForClient.accept();
                    PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
                    Scanner scanner = new Scanner(socket.getInputStream());
                    new Thread(() -> {
                        try {
                            if (scanner.hasNextLine()) {
                                String cmd = scanner.nextLine();
                                if (cmd.equalsIgnoreCase("RANDOM")) {
                                    int count = countSlaves();
                                    if (count == 0) {
                                        writer.println("EMPTY");
                                    } else {
                                        Random rand = new Random();
                                        Socket s = getSlave(rand.nextInt(count));
                                        String slave = s.getInetAddress().toString() + ":" + s.getPort(); // ipaddress::port
                                        writer.println("ACK::" + slave);
                                    }
                                } else {
                                    writer.println("NACK");
                                }
                            }
                            writer.close();
                            scanner.close();
                            socket.close();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }).start();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    private synchronized int generateSlaveId() {
        return SLAVE_ID_COUNTER++;
    }

    private Socket getSlave(int id) {
        synchronized (sem) {
            return slaves.get(id);
        }
    }

    private void removeSlave(int id) {
        synchronized (sem) {
            slaves.remove(id);
        }
    }

    private void addSlave(int id, Socket socket) {
        synchronized (sem) {
            slaves.put(id, socket);
        }
    }

    private int countSlaves() {
        synchronized (sem) {
            return slaves.size();
        }
    }

    private void sendBroadcast(String keyValue, int exceptId) {
        new Thread(() -> slaves.entrySet()
                .stream()
                .filter(entry -> entry.getKey() != exceptId)
                .forEach(entry -> {
                    try {
                        PrintWriter writer = new PrintWriter(entry.getValue().getOutputStream(), true);
                        writer.println("PUTKV::" + keyValue);
                        writer.close();
                        // do not close the socket (connection)
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                })).start();
    }
}
