package movieSender;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Server {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		DatagramSocket socket = new DatagramSocket(4445);
		Map<Integer, MovieSender> clients = new HashMap<>();
		List<String> movies = new ArrayList<String>();
		movies.add("film1 1");
		movies.add("The Martian");
		movies.add("Spectre");

		byte[] buf = new byte[256];
		byte[] id = new byte[2];
		int clientId = id[0] * 256 + id[1];
		while (true) {
			DatagramPacket packet = new DatagramPacket(buf, buf.length);
			socket.receive(packet);
			System.out.println("primen paket");

			id[0] = buf[0];
			id[1] = buf[1];

			if (clients.containsKey(clientId)) {
				// connected before
			} else {
				clients.put(clientId, null);
				System.out.println("dodaden klient so ID " + id[0] + id[1]);
			}

			// extract command
			byte command = buf[2];

			if (command == 0) {// naredba za prakjanje lista od filmovi
				System.out.println("primena naredba za lista od filmovi");
				byte[] buf2 = new byte[] { 0 };

				DatagramPacket p = PametniSme.getMeAPacket(buf2,
						packet.getPort(), packet.getAddress());

				socket.send(p);

				for (String movie : movies) {
					buf2 = movie.getBytes();
					p.setData(buf2, 0, buf2.length);
					// p = new DatagramPacket(buf2, buf2.length);
					// p.setAddress(packet.getAddress());
					// p.setPort(4446);
					socket.send(p);
				}

				buf2 = new String("kraj").getBytes();
				p.setData(buf2, 0, buf2.length);
				socket.send(p);
			} else if (command == 1) { // naredba za prakjanje na film
				int c1 = buf[3];
				int c2 = buf[4];
				int numMovie = c1 * 256 + c2;
				if (numMovie < movies.size()) {
					String reqMovie = movies.get(numMovie);
					System.out.println("pobaran e film " + reqMovie);
					MovieSender sender = new MovieSender(reqMovie,
							packet.getPort(), packet.getAddress(), socket);
					clients.put(clientId, sender);
					sender.start();

				} else {
					System.out.println("klientot ne zeza");
				}

			} else if (command == 2) {// naredba za nova pozicija
				int c1 = buf[3];
				int c2 = buf[4];
				long frameId = c1 * 256 + c2;

				MovieSender sender = clients.get(clientId);
				if (sender != null) {
					sender.setFrameId(frameId);
				} else {
					System.out.println("klientot ne zeza");
				}
			} else if (command == 3) {//naredba za kraj na film
				MovieSender sender = clients.get(clientId);
				if (sender != null) {
					sender.setFrameId(216000);
				} else {
					System.out.println("klientot ne zeza");
				}
			}

		}

	}
}
