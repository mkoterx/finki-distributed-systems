package movieSender;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class MovieSender extends Thread {

	String reqMovie;
	int port;
	InetAddress address;
	Long frameId;
	DatagramSocket socket;

	public MovieSender(String reqMovie, int port, InetAddress address,
			DatagramSocket socket) {
		this.reqMovie = reqMovie;
		this.port = port;
		this.address = address;
		this.socket = socket;
		frameId = new Long(0);
	}

	public void setFrameId(long frameId) {
		synchronized (this.frameId) {
			this.frameId = frameId;
		}

	}

	private Long getFrameId() {
		synchronized (frameId) {
			return frameId;
		}
	}

	public void run() {
		System.out.println("pocnuvam da prakjam film " + reqMovie);

		while (this.getFrameId() < 216000) {
			byte[] buf = this.getFrameId().toString().getBytes();
			DatagramPacket p = new DatagramPacket(buf, buf.length);
			p.setAddress(address);
			p.setPort(port);
			try {
				socket.send(p);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			this.setFrameId(this.getFrameId() + 1);

			try {
				sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("konec filma");
	}

}
