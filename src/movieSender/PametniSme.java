package movieSender;

import java.net.DatagramPacket;
import java.net.InetAddress;

public class PametniSme {
	public static DatagramPacket getMeAPacket(byte[] buffer, int port,
			InetAddress address) {
		DatagramPacket p = new DatagramPacket(buffer, buffer.length);
		p.setPort(port);
		p.setAddress(address);

		return p;

	}
}
