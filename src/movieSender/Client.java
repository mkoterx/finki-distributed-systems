package movieSender;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class Client {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		byte[] buf = new byte[] { 0, 1, 0 };
		DatagramSocket socket = new DatagramSocket(4446);
		DatagramPacket initP = new DatagramPacket(buf, buf.length);
		initP.setAddress(InetAddress.getByName("127.0.0.1"));
		initP.setPort(4445);
		socket.send(initP);
		System.out.println("praten paket za lista od filmovi");

		// primanje lista na filmovi
		boolean flExit = false;
		List<String> movies = new ArrayList<String>();
		while (!flExit) {
			byte[] buf2 = new byte[256];
			DatagramPacket p = new DatagramPacket(buf2, buf2.length);
			socket.receive(p);

			String rez = new String(buf2);

			if (rez.startsWith("kraj")) {
				// kraj na lista na filmovi
				System.out.println("kraj na lista na filmovi");
				flExit = true;
			} else {
				movies.add(rez);
				System.out.println(rez);
			}
		}

		// poraka za pocnuvanje na film
		buf = new byte[] { 0, 1, 1, 0, 2 };
		DatagramPacket reqMovie = new DatagramPacket(buf, buf.length);
		reqMovie.setAddress(InetAddress.getByName("127.0.0.1"));
		reqMovie.setPort(4445);
		socket.send(reqMovie);
		System.out.println("praten paket za pocetok na film");

		int recvFrame = 0;
		int numTries = 0;
		while (true) {
			recvFrame++;

			if (recvFrame == 10) {
				// prati nova pozicija
				buf = new byte[] { 0, 1, 2, 0, 8 };
				DatagramPacket p = PametniSme.getMeAPacket(buf, 4445,
						InetAddress.getByName("127.0.0.1"));
				socket.send(p);
				recvFrame = 0;
				numTries++;
			}

			if (numTries == 5)
				break;

			byte[] buf2 = new byte[256];
			DatagramPacket p = new DatagramPacket(buf2, buf2.length);
			socket.receive(p);
			String frameStr = new String(buf2);
			System.out.println("frame: " + frameStr);
		}

		buf = new byte[] { 0, 1, 3 };
		DatagramPacket p = PametniSme.getMeAPacket(buf, 4445,
				InetAddress.getByName("127.0.0.1"));
		socket.send(p);

	}

}
