package finkiSenzori;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Random;

public class ClientMonitor {

    private static String[] sensors = {};

    public static void main(String[] args) {
        try {
            InetAddress serverAddress = InetAddress.getByName("localhost");
            int serverPort = 8001;

            new Thread(() -> {
                try {
                    while (true) {
                        DatagramSocket socket = new DatagramSocket();
                        byte[] buf = "LIST".getBytes();
                        DatagramPacket packet = new DatagramPacket(buf, buf.length, serverAddress, serverPort);
                        socket.send(packet);

                        byte[] recBuf = new byte[65536];
                        DatagramPacket recPacket = new DatagramPacket(recBuf, recBuf.length, serverAddress, serverPort);
                        socket.receive(recPacket);
                        String response = new String(recPacket.getData(), 0, recPacket.getLength());
                        if (!response.isEmpty()) {
                            String[] list = response.split(";");
                            sensors = list;
                        }
                        System.out.println("Updated list of sensors, number of sensors is " + sensors.length);
                        Thread.sleep(30 * 1000); // refresh every 30 seconds
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();

            Thread.sleep(5 * 1000);

            new Thread(() -> {
                try {
                    while (true) {
                        if (sensors.length == 0) {
                            System.out.println("No room info");
                            Thread.sleep(5 * 1000);
                            continue;
                        }
                        DatagramSocket socket = new DatagramSocket();
                        Random rand = new Random();
                        String sensorId = sensors[rand.nextInt(sensors.length)];
                        String cmd = "INFO::" + sensorId;
                        byte[] buf = cmd.getBytes();
                        DatagramPacket packet = new DatagramPacket(buf, buf.length, serverAddress, serverPort);
                        socket.send(packet);

                        byte[] recBuf = new byte[65536];
                        DatagramPacket recPacket = new DatagramPacket(recBuf, recBuf.length, serverAddress, serverPort);
                        socket.receive(recPacket);
                        String response = new String(recPacket.getData(), 0, recPacket.getLength());
                        if (!response.equalsIgnoreCase("CHOUT")) {
                            SensorInfo sensorInfo = SensorInfo.fromString(response);
                            System.out.println(String.format("Room %d, number of people: %d", sensorInfo.getRoomId(), sensorInfo.getPersonCount()));
                            Thread.sleep(5 * 1000); // refresh every 5 seconds
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
