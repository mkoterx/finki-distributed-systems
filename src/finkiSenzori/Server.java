package finkiSenzori;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Server {

    static Map<String, SensorInfo> personCountPerRoom = new HashMap<>(); // ip-address::port -> roomId::personCount

    public static void main(String[] args) {
        System.out.println("Server started");
        SensorListener sensorListener = new SensorListener(personCountPerRoom);
        sensorListener.start();
        MonitorListener monitorListener = new MonitorListener(personCountPerRoom);
        monitorListener.start();
    }

}

class SensorListener extends Thread {

    private static int LISTENING_PORT = 8000;
    private final Map<String, SensorInfo> personCountPerRoom;

    public SensorListener(Map<String, SensorInfo> personCountPerRoom) {
        this.personCountPerRoom = personCountPerRoom;
    }

    @Override
    public void run() {
        try {
            DatagramSocket socket = new DatagramSocket(LISTENING_PORT);
            System.out.println("Started listening for sensors on port " + LISTENING_PORT);
            while (true) {
                byte[] buf = new byte[3];
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                socket.receive(packet); // blocks
                int cmd = buf[0];
                if (cmd == 0) { // sensor check-out
                    InetAddress address = packet.getAddress();
                    int port = packet.getPort();
                    String sensorId = address.toString() + ":" + port;
                    personCountPerRoom.remove(sensorId);
                    sendResponse(socket, address, port, "ACK");
                    System.out.println(String.format("Check-out: sensor [%s]", sensorId));
                } else if (cmd == 1) { // sensor check-in
                    InetAddress address = packet.getAddress();
                    int port = packet.getPort();
                    String sensorId = address.toString() + ":" + port;
                    int roomId = buf[1] * 127 + buf[2];
                    personCountPerRoom.put(sensorId, new SensorInfo(roomId));
                    sendResponse(socket, address, port, "ACK");
                    System.out.println(String.format("Check-in: sensor [%s] for room: %d", sensorId, roomId));
                } else if (cmd == 2) { // person count update
                    InetAddress address = packet.getAddress();
                    int port = packet.getPort();
                    String sensorId = address.toString() + ":" + port;
                    int personCount = buf[1] * 127 + buf[2];
                    personCountPerRoom.computeIfPresent(sensorId, (id, sensorInfo) -> {
                        sensorInfo.setPersonCount(personCount);
                        return sensorInfo;
                    });
                    System.out.println(String.format("Update for sensor [%s] - current number of people: %d", sensorId, personCount));
                } else {

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private void sendResponse(DatagramSocket socket, InetAddress address, int port, String msg) {
        byte[] buff = msg.getBytes();
        DatagramPacket packet = new DatagramPacket(buff, buff.length, address, port);
        try {
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class MonitorListener extends Thread {

    private static int LISTENING_PORT = 8001;
    private final Map<String, SensorInfo> personCountPerRoom;

    public MonitorListener(Map<String, SensorInfo> personCountPerRoom) {
        this.personCountPerRoom = personCountPerRoom;
    }

    @Override
    public void run() {
        try {
            DatagramSocket socket = new DatagramSocket(LISTENING_PORT);
            System.out.println("Started listening for monitors on port " + LISTENING_PORT);
            while (true) {
                byte[] buf = new byte[65536];
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                socket.receive(packet); // blocks
                String cmd = new String(packet.getData(), 0, packet.getLength());
                if (cmd.equalsIgnoreCase("LIST")) {
                    InetAddress address = packet.getAddress();
                    int port = packet.getPort();
                    String response = listSensors();
                    sendResponse(socket, address, port, response);
                    System.out.println(String.format("Sending list of sensors to %s:%d", address.toString(), port));
                } else if (cmd.startsWith("INFO")) {
                    InetAddress address = packet.getAddress();
                    int port = packet.getPort();
                    String sensorId = cmd.split("::")[1];
                    SensorInfo sensorInfo = personCountPerRoom.get(sensorId);
                    String response;
                    if (sensorInfo != null) {
                        response = sensorInfo.toString();
                    } else {
                        response = "CHOUT";
                    }
                    sendResponse(socket, address, port, response);
                    System.out.println(String.format("Sending info for server [%s] to %s%d", sensorId, address, port));
                } else {

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendResponse(DatagramSocket socket, InetAddress address, int port, String msg) {
        byte[] buff = msg.getBytes();
        DatagramPacket packet = new DatagramPacket(buff, buff.length, address, port);
        try {
            socket.send(packet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String listSensors() {
        return personCountPerRoom.keySet().stream().collect(Collectors.joining(";"));
    }
}

