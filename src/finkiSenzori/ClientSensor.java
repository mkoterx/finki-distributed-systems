package finkiSenzori;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ClientSensor {

    private static int PORT = 5001;
    private static int ROOM_ID = 322;

    public static void main(String[] args) {
        try {
            InetAddress serverAddress = InetAddress.getByName("localhost");
            int serverPort = 8000;

            DatagramSocket socket = new DatagramSocket(PORT);
            checkIn(socket, serverAddress, serverPort);

            byte[] recBuf = new byte[65536];
            DatagramPacket recPacket = new DatagramPacket(recBuf, recBuf.length);
            socket.receive(recPacket);
            String response = new String(recPacket.getData(), 0, recPacket.getLength());
            if (response.equalsIgnoreCase("ACK")) {
                sendCountUpdate(socket, serverAddress, serverPort, 12);
                Thread.sleep(3 * 1000);
                sendCountUpdate(socket, serverAddress, serverPort, 23);
                Thread.sleep(3 * 1000);
                sendCountUpdate(socket, serverAddress, serverPort, 7);
                Thread.sleep(3 * 1000);
                sendCountUpdate(socket, serverAddress, serverPort, 8);
                Thread.sleep(3 * 1000);

//                checkOut(socket, serverAddress, serverPort);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void sendCountUpdate(DatagramSocket socket, InetAddress address, int port, int count) {
        try {
            byte[] buf = new byte[3];
            buf[0] = 2; // update count info command
            buf[1] = (byte) (count / 127);
            buf[2] = (byte) (count % 127);
            DatagramPacket packet = new DatagramPacket(buf, buf.length, address, port);
            socket.send(packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void checkIn(DatagramSocket socket, InetAddress address, int port) throws IOException {
        byte[] buf = new byte[3];
        buf[0] = 1; // check=in command
        buf[1] = (byte) (ROOM_ID / 127);
        buf[2] = (byte) (ROOM_ID % 127);
        DatagramPacket packet = new DatagramPacket(buf, buf.length, address, port);
        socket.send(packet);
    }

    private static void checkOut(DatagramSocket socket, InetAddress address, int port) throws IOException {
        byte[] buf = new byte[1];
        buf[0] = 0; // check=in command
        DatagramPacket packet = new DatagramPacket(buf, buf.length, address, port);
        socket.send(packet);
    }
}
