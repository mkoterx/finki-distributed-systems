package finkiSenzori;

public class SensorInfo {
    private int roomId;
    private int personCount;

    public SensorInfo(int roomId) {
        this.roomId = roomId;
    }

    public SensorInfo(int roomId, int personCount) {
        this.roomId = roomId;
        this.personCount = personCount;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public void setPersonCount(int personCount) {
        this.personCount = personCount;
    }

    public int getRoomId() {
        return roomId;
    }

    public int getPersonCount() {
        return personCount;
    }

    @Override
    public String toString() {
        return roomId + ":" + personCount;
    }

    public static SensorInfo fromString(String str) {
        String[] parts = str.split(":");
        int roomId = Integer.parseInt(parts[0]);
        int personCount = Integer.parseInt(parts[1]);
        return new SensorInfo(roomId, personCount);
    }
}

