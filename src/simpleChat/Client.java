package simpleChat;

import java.net.InetAddress;
import java.net.Socket;

public class Client {

    public static void main(String[] args) {
        try {
            Socket socket = new Socket(InetAddress.getLocalHost().getHostAddress(), 8000);
            new WritingThread(socket.getOutputStream()).start();
            new ReadingThread(socket.getInputStream()).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}