package simpleChat;

import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(8000);
            Socket socket = serverSocket.accept();
            System.out.println("client1 connected");

            new WritingThread(socket.getOutputStream()).start();
            new ReadingThread(socket.getInputStream()).start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}