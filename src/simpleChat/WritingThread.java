package simpleChat;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class WritingThread extends Thread {

    private final OutputStream outputStream;

    public WritingThread(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    @Override
    public void run() {
        PrintWriter pw = new PrintWriter(outputStream, true);
        Scanner sc = new Scanner(System.in);

        while (true) {
            if (sc.hasNextLine()) {
                String msg = sc.nextLine();
                pw.println(msg);
            }
        }
    }
}
