package simpleChat;

import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class ReadingThread extends Thread {

    private final InputStream inputStream;

    ReadingThread(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    @Override
    public void run() {
        Scanner scanner = new Scanner(inputStream);
        PrintWriter writer = new PrintWriter(System.out, true);
        while (true) {
            if (scanner.hasNextLine()) {
                String msg = scanner.nextLine();
                writer.println(msg);
            }
        }
    }
}
